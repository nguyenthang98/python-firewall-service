#!/bin/bash
function log {
	echo -e "\e[92m$@\e[39m"
}

[ "$UID" -eq 0 ] || exec sudo "$0" "$@"
#
log "Installing dkms"

([[ -f /usr/bin/yum ]] && yum install -y "kernel-devel-uname-r == $(uname -r)" && \
    yum install -y kernel-devel epel-release dkms patch) || \
    ([[ -f /usr/bin/pacman ]] && pacman -S --noconfirm dkms) || \
    ([[ -f /usr/bin/apt ]] && apt install --assume-yes dkms)

which dkms
if [ $? -eq 1 ]
then
	echo "Package dkms has not been installed in your system. Please install it from your package manager"
	exit 1
fi


if $(dkms status | grep -q kpcre)
then 
	log "found kpcre module"
else
	log "not found kpcre module. building module"
	if [ -d /tmp/kpcre ]
	then 
		rm -rf /tmp/kpcre
	fi
	pushd /tmp
	git clone https://github.com/xnsystems/kpcre.git
	mv -f kpcre /usr/src/kpcre-1.0.0
	dkms add -m kpcre -v 1.0.0
	dkms build -m kpcre -v 1.0.0
	dkms install -m kpcre -v 1.0.0
	popd
fi

# python2.7 -m pip install -r requirements.txt
# git submodule update --init

# download pattern files
# pushd ./src/protocols
# wget -r -np -nH --cut-dirs=3 -R *index.html* http://l7-filter.sourceforge.net/layer7-protocols/protocols/
# popd
