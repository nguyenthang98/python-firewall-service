#!/bin/bash
function log {
	echo -e "\e[92m$@\e[39m"
}

[ "$UID" -eq 0 ] || exec sudo "$0" "$@"

cp ../fw-service /usr/bin/

cp ./fw-service.service /usr/lib/systemd/system/

log "Setting up environment for fw-service..."
./setup-environment.sh

log "Starting fw-service..."
systemctl daemon-reload
systemctl enable fw-service
systemctl stop fw-service
systemctl start fw-service

# integrate with openstack
log "Integrating fw-service into OpenStack..."
cp ./openstack-dashboard.diff /usr/share/openstack-dashboard/
pushd /usr/share/openstack-dashboard
patch -p0 < openstack-dashboard.diff
popd

log "Restarting httpd..."
systemctl stop httpd
systemctl start httpd
