from flask import Flask, render_template, request, redirect, url_for, make_response, jsonify, abort, send_from_directory
from werkzeug.exceptions import HTTPException
from simplepam import authenticate
from firewall import Firewall
import jwt, json, re, traceback, os
import logging

app = Flask(__name__, static_folder="public/firewall-front-end/build")
app.url_map.strict_slashes = False

JWT_SECRET = "secret"
JWT_ALGO = "HS256"

HTTP_CODE = {
        "GET_SUCCESS": 200,
        "POST_SUCCESS": 201,
        "UNAUTHORIZED": 401,
        "SERVER_ERROR": 500,
        "NOT_IMPLEMENTED": 501
}

def make_json_response(data, status_code, headers=None):
    response = make_response(jsonify(data), status_code)
    if headers:
        for k, v in headers.items():
            response.headers[k] = v
    return response

@app.before_request
def auth():
    print('host_url: {}, endpoint: {}, url: {}, path: {}'.format(request.host_url, request.endpoint, request.url, request.path))

    # firewall api route
    if re.match("^/firewall($|/)", request.path):
        # DO AUTHENTICATION
        # check token
        token = request.headers.get("Authorization")
        if token:
            try:
                decoded_token = jwt.decode(token, JWT_SECRET, JWT_ALGO)
                user = str(decoded_token['uname'])
                pwd = str(decoded_token['pwd'])
                if not authenticate(user, pwd):
                    print("authentication error")
                    abort(HTTP_CODE[UNAUTHORIZED], "Authentication error")
            except Exception as error:
                abort(HTTP_CODE["UNAUTHORIZED"], error.message)
        else:
            abort(HTTP_CODE["UNAUTHORIZED"], "No token provided")

"""
PUBLIC ROUTES
"""
@app.route('/', methods=["GET"], defaults={'path': ''})
@app.route('/<path:path>', methods=["GET"])
def serve_client_side(path):
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        print("sending {path} from folder {folder}".format(path=path, folder=app.static_folder))
        return send_from_directory(app.static_folder, path)
    else:
        return app.send_static_file("index.html")

@app.route('/auth/login', methods=["POST"])
def login():
    _cre = request.get_json()
    user_name = str(_cre["uname"])
    password = str(_cre["pwd"])
    if authenticate(user_name, password):
        payload = {"uname": user_name, "pwd": password}
        encoded_token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGO)
        return make_json_response({"message": "Authentication successfully", "token": encoded_token}, HTTP_CODE["POST_SUCCESS"], {"Authorization": encoded_token})
    else:
        return make_json_response({"message": "Authentication failed"}, HTTP_CODE["UNAUTHORIZED"])

@app.errorhandler(Exception)
def exception(e):
    traceback.print_exc()
    if isinstance(e, HTTPException):
        data = { "name": e.name, "message": e.message or e.description, "code": e.code }
    else:
        data = { "name": "Internal Server ERROR", "message": e.message or e.description, "code": HTTP_CODE["SERVER_ERROR"] }
    return make_json_response(data, data["code"])

"""
AUTHENTICATION REQUIRED ROUTES
"""
# get all firewall
@app.route("/firewall", methods=["GET"])
@app.route("/firewall/<string:nsname>", methods=["GET"])
def firewall(nsname=None):
    if not nsname:
        return make_json_response(Firewall.get_all_ns(), HTTP_CODE['GET_SUCCESS'])
    else:
        if request.method == "GET":
            return make_json_response(Firewall(nsname).get_firewalls(), HTTP_CODE['GET_SUCCESS'])

@app.route("/firewall/<string:nsname>/<string:layer_name>/toggle", methods=["GET"])
def toggle_layer(nsname=None, layer_name=None):
    if not nsname:
        raise ValueError("Namespace must be provided")
    if not layer_name:
        raise ValueError("Layer must be specified")
    else:
        fw = Firewall(nsname)
        layer_fw = fw.get_layer_inst(layer_name)
        fw.toggle_layer(layer_name)
        data = { "rules": layer_fw.get_rules(), "use": fw.is_used(layer_name)}
        return make_json_response(data, HTTP_CODE["GET_SUCCESS"])

@app.route("/firewall/<string:nsname>/interfaces", methods=["GET"])
def list_interfaces(nsname=None):
    if not nsname:
        raise ValueError("Namespace must be provided")
    else:
        fw = Firewall(nsname)
        return make_json_response(fw.l2.list_interfaces(), HTTP_CODE["GET_SUCCESS"])

@app.route("/firewall/<string:nsname>/<string:layer_name>/protocols", methods=["GET"])
def list_protocols(nsname=None, layer_name=None):
    if not nsname:
        raise ValueError("Namespace must be provided")
    if not layer_name:
        raise ValueError("layer_name must be provided")
    else:
        fw = Firewall(nsname)
        layer_fw = fw.get_layer_inst(layer_name)
        if "list_protocols" in dir(layer_fw):
            return make_json_response(layer_fw.list_protocols(), HTTP_CODE["GET_SUCCESS"])
        else:
            raise ValueError("Layer {} does not support list protocols function".format(layer_name))

# get all rules from l2 firewall
@app.route("/firewall/<string:nsname>/<string:layer_name>", methods=["GET", "POST", "PUT", "DELETE"])
def firewall_on_layer(nsname=None, layer_name=None):
    if not nsname:
        raise ValueError("Namespace must be provided")
    if not layer_name:
        raise ValueError("Layer must be specified")
    else:
        fw = Firewall(nsname)
        layer_fw = fw.get_layer_inst(layer_name)

        if not layer_fw:
            raise ValueError("Filter layer {} not found".format(layer_num))

        if request.method == "GET":
            data = { "rules": layer_fw.get_rules(), "use": fw.is_used(layer_name)}
            return make_json_response(data, HTTP_CODE["GET_SUCCESS"])
        elif request.method == "POST":
            data = request.get_json()
            rules = list(data["rules"]) if data.has_key("rules") else None
            layer_fw.add_rules(rules)
            res = { "rules": layer_fw.get_rules(), "use": fw.is_used(layer_name)}
            return make_json_response(res, HTTP_CODE["POST_SUCCESS"])
        elif request.method == "PUT":
            data = request.get_json()
            old_rules = dict(data["old_rules"]) if data.has_key("old_rules") else None
            new_rules = dict(data["new_rules"]) if data.has_key("new_rules") else None
            layer_fw.change_rules(old_rules, new_rules)
            res = { "rules": layer_fw.get_rules(), "use": fw.is_used(layer_name)}
            return make_json_response(res, HTTP_CODE["POST_SUCCESS"])
        elif request.method == "DELETE":
            data = request.get_json()
            rules = list(data["rules"])
            layer_fw.delete_rules(rules)
            res = { "rules": layer_fw.get_rules(), "use": fw.is_used(layer_name)}
            return make_json_response(res, HTTP_CODE["POST_SUCCESS"])

def run_server(port):
    app.run(host="0.0.0.0", port=port)
