import os, subprocess, fcntl, struct, array, socket
from ctypes import CDLL, get_errno
from ast import literal_eval

CLONE_NEWNET = 0x40000000

def errcheck(ret, func, args):
    if ret == -1:
        errno = get_errno()
        raise OSError(errno, os.strerror(errno))

libc = CDLL('libc.so.6', use_errno=True)
libc.setns.errcheck = errcheck

def get_ns_path(nsname=None):
    if not nsname:
        raise ValueError("namepace name not found")
    nspath = '/var/run/netns/{}'.format(nsname)
    if not os.path.exists(nspath):
        raise ValueError("namespace path {} does not exists".format(nspath))
    return nspath

def namespace_exec(nsname=None, handler=None, **kwargs):
    if not hasattr(handler, '__call__'):
        raise ValueError("Handler must be an executable function")
    # file descriptor for interprocess communication
    r, w = os.pipe()
    # into namespace spawn new child process and run handler function
    pid = os.fork()
    if pid == 0:
        os.close(r)
        # _w = os.fdopen(w, 'w')
        if nsname and nsname.lower() != "default":
            nspath = get_ns_path(nsname)
            fd = os.open(nspath, os.O_RDONLY)
            libc.setns(fd, CLONE_NEWNET)
            # child process space execute handler
            ret = handler(**kwargs)
            os.write(w, str(ret))
            os._exit(0)
        else:
            ret = handler(**kwargs)
            os.write(w, str(ret))
            os._exit(0)
    else:
        os.waitpid(pid, 0)
        os.close(w)
        ret = ""
        with os.fdopen(r, 'r') as f:
            ret += f.read()
        # print("parent process", ret)
        try:
            dic = literal_eval(ret)
            # print("decripted", dic)
            return dic
        except:
            # print("error while decripting")
            return ret

def list_ns():
    # return ["default"] + subprocess.check_output(["ip", "netns"]).splitlines()
    print("use check output");
    return ["default"] + subprocess.check_output(["ls", "/var/run/netns"]).splitlines()

def format_ip(ip_addr):
    return str(ord(ip_addr[0])) + '.' + \
           str(ord(ip_addr[1])) + '.' + \
           str(ord(ip_addr[2])) + '.' + \
           str(ord(ip_addr[3]))


def list_interfaces():
    max_possible = 128  # arbitrary. raise if needed.
    bytes = max_possible * 32
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    names = array.array('B', '\0' * bytes)
    outbytes = struct.unpack('iL', fcntl.ioctl(
        s.fileno(),
        0x8912,  # SIOCGIFCONF
        struct.pack('iL', bytes, names.buffer_info()[0])
    ))[0]
    namestr = names.tostring()
    lst = []
    for i in range(0, outbytes, 40):
        name = namestr[i:i+16].split('\0', 1)[0]
        ip   = namestr[i+20:i+24]
        lst.append({
            "name": str(name),
            "ip": format_ip(ip)
        })
    return lst

def list_dir(folder_path=None):
    if not os.path.exists(folder_path):
        raise ValueError("protocols path {} is invalid".format(folder_path))
    return os.listdir(folder_path)

def get_file_name(file_name=None):
    if not file_name:
        raise ValueError("file_name must be provided")
    return os.path.splitext(file_name)[0]

def read_pat_file(file_path):
    pattern = {}
    with open(file_path, 'r') as file:
        line_idx = 0
        for line in file:
            _line = line.lstrip()
            if line_idx == 0:
                pattern.update({'description': _line.strip()})
            line_idx += 1

            # ignore empty line and comment
            if len(_line) == 0 or _line[0] == "#":
                continue
            else:
                if not pattern.has_key('name'):
                    pattern.update({'name': _line.strip()})
                else:
                    pattern.update({'pattern': _line.strip().replace(" ", "\ ")})

    # print("find pattern from file {}: ".format(file_path), pattern)
    return pattern
