from utils import namespace_exec, list_ns, list_interfaces
import iptc

WORKING_TABLE = "filter"
FIREWALL_CHAIN_NAME = 'firewall-chain'
LAYER2_CHAIN_NAME = 'firewall-l2-chain'
LAYER3_CHAIN_NAME = 'firewall-l3-chain'
LAYER4_CHAIN_NAME = 'firewall-l4-chain'
LAYER7_CHAIN_NAME = 'firewall-l7-chain'

l2_rule_d = {'target': LAYER2_CHAIN_NAME}
l3_rule_d = {'target': LAYER3_CHAIN_NAME}
l4_rule_d = {'target': LAYER4_CHAIN_NAME}
l7_rule_d = {'target': LAYER7_CHAIN_NAME}

LAYER2_NAME = "l2"
LAYER3_NAME = "l3"
LAYER4_NAME = "l4"
LAYER7_NAME = "l7"

PIORITY_TABLE = {
	"l2": 1,
	"l3": 2,
	"l4": 3,
	"l7": 4,
	"firewall-l2-chain": 1,
	"firewall-l3-chain": 2,
	"firewall-l4-chain": 3,
	"firewall-l7-chain": 4,
}

class Firewall:
    def __init__(self, nsname=None):
        self.nsname = nsname
        namespace_exec(nsname, self.init_firewall)
        self.l2 = FilterLayer2(nsname)
        self.l3 = FilterLayer3(nsname)
        self.l4 = FilterLayer4(nsname)
        self.l7 = FilterLayer7(nsname)

    def init_firewall(self, **kargs):
        # inside namespace's scope
        # check and create necessary chains
        if not iptc.easy.has_chain('filter', FIREWALL_CHAIN_NAME):
            iptc.easy.add_chain('filter', FIREWALL_CHAIN_NAME)
        if not iptc.easy.has_chain('filter', LAYER2_CHAIN_NAME):
            iptc.easy.add_chain('filter', LAYER2_CHAIN_NAME)
        if not iptc.easy.has_chain('filter', LAYER3_CHAIN_NAME):
            iptc.easy.add_chain('filter', LAYER3_CHAIN_NAME)
        if not iptc.easy.has_chain('filter', LAYER4_CHAIN_NAME):
            iptc.easy.add_chain('filter', LAYER4_CHAIN_NAME)
        if not iptc.easy.has_chain('filter', LAYER7_CHAIN_NAME):
            iptc.easy.add_chain('filter', LAYER7_CHAIN_NAME)

        # set dataflow to custom chains
        fw_rule_d = {'target': FIREWALL_CHAIN_NAME}

        if not iptc.easy.has_rule('filter', 'INPUT', fw_rule_d):
            iptc.easy.add_rule('filter', 'INPUT', fw_rule_d, position=1)
        if not iptc.easy.has_rule('filter', 'FORWARD', fw_rule_d):
            iptc.easy.add_rule('filter', 'FORWARD', fw_rule_d, position=1)

        return 0

    def get_firewalls(self):
        # inside namespace's scope
        # get all firewall rules
        def _get_firewalls():
            fws_d = {}
            fws_d.update({"l2": {
                    "rules": self.l2.get_rules(),
                    "use": self.is_used(LAYER2_NAME)
                }
            })
            fws_d.update({"l3": {
                    "rules": self.l3.get_rules(),
                    "use": self.is_used(LAYER3_NAME)
                }
            })
            fws_d.update({"l4": {
                    "rules": self.l4.get_rules(),
                    "use": self.is_used(LAYER4_NAME)
                }
            })
            fws_d.update({"l7": {
                    "rules": self.l7.get_rules(),
                    "use": self.is_used(LAYER7_NAME)
                }
            })
            return fws_d

        fws_d = namespace_exec(self.nsname, _get_firewalls)
        return fws_d

    def is_used(self, layer_name):
        def _is_used():
            if layer_name == LAYER2_NAME:
                return 1 if iptc.easy.has_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l2_rule_d) else 0
            elif layer_name == LAYER3_NAME:
                return 1 if iptc.easy.has_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l3_rule_d) else 0
            elif layer_name == LAYER4_NAME:
                return 1 if iptc.easy.has_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l4_rule_d) else 0
            elif layer_name == LAYER7_NAME:
                return 1 if iptc.easy.has_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l7_rule_d) else 0
            else: raise ValueError("Filter Layer {} not found".format(layer_name))
        return namespace_exec(self.nsname, _is_used)

    def toggle_layer(self, layer_name):
        def _toggle_layer():
            if layer_name == LAYER2_NAME:
                if iptc.easy.has_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l2_rule_d):
                    iptc.easy.delete_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l2_rule_d)
                else:
                    iptc.easy.add_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l2_rule_d, position=self.l2.get_chain_position())
            elif layer_name == LAYER3_NAME:
                if iptc.easy.has_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l3_rule_d):
                    iptc.easy.delete_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l3_rule_d)
                else:
                    iptc.easy.add_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l3_rule_d, position=self.l3.get_chain_position())
            elif layer_name == LAYER4_NAME:
                if iptc.easy.has_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l4_rule_d):
                    iptc.easy.delete_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l4_rule_d)
                else:
                    iptc.easy.add_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l4_rule_d, position=self.l4.get_chain_position())
            elif layer_name == LAYER7_NAME:
                if iptc.easy.has_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l7_rule_d):
                    iptc.easy.delete_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l7_rule_d)
                else:
                    iptc.easy.add_rule(WORKING_TABLE, FIREWALL_CHAIN_NAME, l7_rule_d, position=self.l7.get_chain_position())
            else: raise ValueError("Filter Layer {} not found".format(layer_name))

        return namespace_exec(self.nsname, _toggle_layer)

    def get_layer_inst(self, layer_name=None):
        if not layer_name:
            raise ValueError("Filter Layer not found")
        elif layer_name == LAYER2_NAME: return self.l2
        elif layer_name == LAYER3_NAME: return self.l3
        elif layer_name == LAYER4_NAME: return self.l4
        elif layer_name == LAYER7_NAME: return self.l7
        else: raise ValueError("Filter Layer {} not found".format(layer_name))

    @staticmethod
    def get_all_ns():
        return list_ns()

class FilterLayer(object):
    def __init__(self, nsname=None):
        self.nsname=nsname

    def _get_chain_name(self):
        raise NotImplementedError

    # from kernel rule => application rule
    def _encode_rule(self, rule_d):
        raise NotImplementedError

    # from application rule => kernel rule
    def _decode_rule(self, rule_d):
        raise NotImplementedError

    def get_rules(self):
        def _get_rules():
            rules = iptc.easy.dump_chain(WORKING_TABLE, self._get_chain_name())
            for i, rule_d in enumerate(rules):
                if isinstance(rule_d["target"], dict) and rule_d["target"].has_key("REJECT"):
                    print("REJECT RULES: ", rule_d)
                    rule_d["target"] = "REJECT"
                rules[i] = self._encode_rule(rule_d)
            return rules
        return namespace_exec(self.nsname, _get_rules)

    def add_rules(self, rules):
        for rule_d in rules:
            self.add_rule(dict(rule_d), 1)

    def change_rules(self, old_rules, new_rules):
        if not old_rules:
            raise ValueError("Old rules not found");
        if not new_rules:
            raise ValueError("New rules not found");

        for k, old_d in old_rules.items():
            self.change_rule(old_d, new_d=new_rules[k])

    def get_chain_position(self):
        def _get_chain_position():
            rules = iptc.easy.dump_chain(WORKING_TABLE, FIREWALL_CHAIN_NAME)
            for position, rule in enumerate(rules):
                if rule["target"] == self._get_chain_name():
                    return position + 1
                elif PIORITY_TABLE[rule["target"]] > PIORITY_TABLE[self._get_chain_name()]:
                    return position + 1
            return 0

        return namespace_exec(self.nsname, _get_chain_position)

    def delete_rules(self, rules):
        for rule_d in rules:
            self.delete_rule(rule_d)

    def add_rule(self, rule_d, position):
        if not rule_d:
            raise ValueError("add rule error, rule_d must be provided")
        def _add_rule():
            decoded_rule_d = self._decode_rule(rule_d)
            if not iptc.easy.has_rule(WORKING_TABLE, self._get_chain_name(), decoded_rule_d):
                iptc.easy.add_rule(WORKING_TABLE, self._get_chain_name(), decoded_rule_d, position=position)
            return 0

        namespace_exec(self.nsname, _add_rule)

    def change_rule(self, old_d=None, new_d=None):
        if not old_d or not new_d:
            raise ValueError("change rule error, old rule or new rule must be provided")
        def _change_rule():
            old_decoded_d = self._decode_rule(old_d)
            new_decoded_d = self._decode_rule(new_d)
            print("old", old_decoded_d)
            print("new", new_decoded_d)
            print("total", iptc.easy.dump_chain(WORKING_TABLE, self._get_chain_name()))
            if not iptc.easy.has_rule(WORKING_TABLE, self._get_chain_name(), old_decoded_d):
                raise ValueError("Old rule is not in rule list")
            iptc.easy.replace_rule(WORKING_TABLE, self._get_chain_name(), old_rule_d=old_decoded_d, new_rule_d=new_decoded_d)
            return 0

        namespace_exec(self.nsname, _change_rule)

    def delete_rule(self, rule_d):
        def _delete_rule():
            decoded_d = self._decode_rule(rule_d)
            iptc.easy.delete_rule(WORKING_TABLE, self._get_chain_name(), decoded_d)
            return 0

        namespace_exec(self.nsname, _delete_rule)

class FilterLayer2(FilterLayer):

    def _get_chain_name(self):
        return LAYER2_CHAIN_NAME

    def list_interfaces(self):
        def _list_interfaces():
            return list_interfaces()
        interfaces = namespace_exec(self.nsname, _list_interfaces)
        return interfaces

    def _encode_rule(self, rule_d):
        _rule_d = {}
        if rule_d.has_key("mac"):
            _rule_d.update({"mac": rule_d["mac"]})

        if rule_d.has_key("in-interface") and rule_d["in-interface"] and len(rule_d["in-interface"]) > 0:
            _rule_d.update({"interface": rule_d["in-interface"]})
        elif rule_d.has_key("out-interface") and rule_d["out-interface"] and len(rule_d["out-interface"]) > 0:
            _rule_d.update({"interface": rule_d["out-interface"]})

        if rule_d.has_key("target"):
            _rule_d.update({"target": rule_d["target"]})

        if _rule_d.has_key("target") and isinstance(rule_d["target"], dict) and _rule_d["target"].has_key("REJECT"):
            _rule_d.update({"target": "REJECT"})

        return _rule_d

    def _decode_rule(self, rule_d):
        _rule_d = {}
        # mac filter only
        if rule_d.has_key("mac"):
            _rule_d.update({"mac": rule_d["mac"]})

        if rule_d.has_key("interface") and rule_d["interface"] and len(rule_d["interface"]) > 0:
            if rule_d["interface"] == "any":
                del rule_d["interface"]
            else:
                _rule_d.update({"in-interface": rule_d["interface"]})
                _rule_d.update({"out-interface": rule_d["interface"]})

        if rule_d.has_key("target"):
            _rule_d.update({"target": rule_d["target"]})

        if _rule_d.has_key("target") and _rule_d["target"] == "REJECT":
            _rule_d.update({"target": {"REJECT": {'reject-with': 'icmp-port-unreachable'}}})

        print("decoded", _rule_d)
        return _rule_d

class FilterLayer3(FilterLayer):

    def _get_chain_name(self):
        return LAYER3_CHAIN_NAME

    def _encode_rule(self, rule_d):
        if rule_d.has_key("target") and isinstance(rule_d["target"], dict) and rule_d["target"].has_key("REJECT"):
            rule_d.update({"target": "REJECT"})
        return rule_d

    def _decode_rule(self, rule_d):
        _rule_d = {}
        if rule_d.has_key("protocol"):
            if rule_d["protocol"] == "any" or not rule_d["protocol"]:
                del rule_d["protocol"]
            else:
                _rule_d.update({"protocol": rule_d["protocol"]})
        if rule_d.has_key("src"):
            if rule_d["src"] == "any":
                del rule_d["src"]
            else:
                _rule_d.update({"src": rule_d["src"]})
        if rule_d.has_key("dst"):
            if rule_d["dst"] == "any":
                del rule_d["dst"]
            else:
                _rule_d.update({"dst": rule_d["dst"]})
        if rule_d.has_key("target"):
            _rule_d.update({"target": rule_d["target"]})

        if _rule_d.has_key("target") and _rule_d["target"] == "REJECT":
            _rule_d.update({"target": {"REJECT": {'reject-with': 'icmp-port-unreachable'}}})

        return _rule_d

    def list_protocols(self):
        return [
            {"protocol": "icmp", "description": "options: {'icmp-type': type[/code]}"},
        ]

class FilterLayer4(FilterLayer):

    def _get_chain_name(self):
        return LAYER4_CHAIN_NAME

    def _encode_rule(self, rule_d):
        if rule_d.has_key("target") and isinstance(rule_d["target"], dict) and rule_d["target"].has_key("REJECT"):
            rule_d.update({"target": "REJECT"})
        return rule_d

    def _decode_rule(self, rule_d):
        _rule_d = {}
        if rule_d.has_key("protocol") and rule_d["protocol"] != "any":
            _rule_d.update({"protocol": rule_d["protocol"]})
            if rule_d.has_key(rule_d["protocol"]):
                key = rule_d["protocol"]
                _rule_d.update({key: rule_d[key]})
        if rule_d.has_key("target"):
            _rule_d.update({"target": rule_d["target"]})

        if _rule_d.has_key("target") and _rule_d["target"] == "REJECT":
            _rule_d.update({"target": {u'REJECT': {'reject-with': 'icmp-port-unreachable'}}})

        return _rule_d

    def list_protocols(self):
        return [
                {"protocol": "tcp", "label": "TCP", "description": "options: ['tcp-flags', 'syn', 'tcp-option']"},
                {"protocol": "udp", "label": "TCP", "description": "udp protocol does not have any option"},
                {"protocol": "sctp", "label": "SCTP", "description": "options: ['chunk-types']"},
        ]

class FilterLayer7(FilterLayer):

    def _get_chain_name(self):
        return LAYER7_CHAIN_NAME

    def _encode_rule(self, rule_d):
        if rule_d.has_key("target") and isinstance(rule_d["target"], dict) and rule_d["target"].has_key("REJECT"):
            rule_d.update({"target": "REJECT"})

        return rule_d

    def _decode_rule(self, rule_d):
        _rule_d = {}
        if rule_d.has_key("string"):
            _rule_d.update({"string": rule_d["string"]})
        if rule_d.has_key("target"):
            _rule_d.update({"target": rule_d["target"]})

        if _rule_d.has_key("target") and _rule_d["target"] == "REJECT":
            _rule_d.update({"target": {"REJECT": {'reject-with': 'icmp-port-unreachable'}}})

        return _rule_d
