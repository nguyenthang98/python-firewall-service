"""
Service deamon run in systemd
response to manage iptables rules

resources
    - supported protocols: http://www.iana.org/assignments/protocol-numbers/protocol-numbers-1.csv

"""
"""
user model
    - l2 :
        -- match mac -- mac-source
    - l3 :
        -- match state --state ESTABLISED,RELATED,NEW, INVALID, UNTRACKED, SNAT, DNAT
        -- protocol list of available protocols
        -saddr
        -daddr

    -l4:
        -sport
        -dport

    - l7 :
        --match through pattern
"""

from endpoint_server import run_server
from firewall import Firewall
import argparse, os, logging

logging.basicConfig(filename="/var/log/fw-service.log")

parser = argparse.ArgumentParser(description="Firewall service.")
parser.add_argument("--port", "-p", type=int, default=3000, help="Port number that service could running on.")

def init_service():
    """
    TODO:
         - CREATE an endpoint server
         - IMPLEMENT FUNCTIONALITY
    """

    """
    fw = Firewall("samplens")
    fw.l2_filter(src_mac="11:22:33:44:55:66", action="DROP")
    fw.l2_filter(src_mac="11:22:33:44:55:66", action='DROP')
    fw.l3_filter(protocol="tcp", src_addr='192.168.0.45', dst_addr='192.168.0.23', action="ACCEPT")
    fw.l4_filter(protocol='tcp', sport='57', dport='58', action="REJECT")
    fw.l4_filter(protocol='tcp', sport='58', dport='57', action="REJECT")
    fw.l7_filter(packet_type="http", action="DROP")
    """

    args = parser.parse_args()
    # start an endpoint server
    run_server(port=args.port)


    ## DEPRECATED
    # NEED MORE TIME TO FOCUS ON THIS FUNCTIONALITY
    #
    # fw.l7_filter(packet_type="validcertssl", action="ACCEPT")
    # for prot in fw.get_l7_protocols():
    #     fw.l7_filter(packet_type=prot, action="ACCEPT")

if __name__ == "__main__":
    init_service()
