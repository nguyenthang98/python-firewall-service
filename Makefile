all: configuration
	pyinstaller --onefile \
		--add-binary '/usr/lib64/python2.7/site-packages/libxtwrapper.so:.' \
		--add-data 'src/public/firewall-front-end/build:public/firewall-front-end/build' \
		--distpath dist/ \
		--noconfirm \
		--name fw-service \
		src/service.py

delivery: all
	scp -r dist thangnguyen@192.168.0.33:/home/thangnguyen

configuration:
	cp -r public dist
