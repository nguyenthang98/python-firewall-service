# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['src/service.py'],
             pathex=['/home/thangnguyen/python-firewall-service'],
             binaries=[('/usr/lib64/python2.7/site-packages/libxtwrapper.so', '.')],
             datas=[('src/public/firewall-front-end/build', 'public/firewall-front-end/build')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='fw-service',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True )
